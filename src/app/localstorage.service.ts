import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {

  private key = 'contacts';

  constructor() { }

  getContacts() {
    const contacts = localStorage.getItem(this.key);
    return contacts ? JSON.parse(contacts) : [];
  }

  getContact(id: number) {
    const contacts = this.getContacts();
    return contacts.find((contact: any) => contact.id === id);
  }

  addContact(contact: any) {
    const contacts = this.getContacts();
    contact.id = contacts.length ? Math.max(...contacts.map((o: any) => o.id)) + 1 : 1;
    contacts.push(contact);
    localStorage.setItem(this.key, JSON.stringify(contacts));
  }

  updateContact(id: number, updatedContact: any) {
    let contacts = this.getContacts();
    contacts = contacts.map((contact: any) =>
      contact.id === id ? { ...contact, ...updatedContact } : contact
    );
    localStorage.setItem(this.key, JSON.stringify(contacts));
  }

  deleteContact(id: number) {
    let contacts = this.getContacts();
    contacts = contacts.filter((contact: any) => contact.id !== id);
    localStorage.setItem(this.key, JSON.stringify(contacts));
  }
}
