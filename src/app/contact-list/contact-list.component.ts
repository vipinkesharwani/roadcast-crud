import { Component, OnInit, ViewChild } from '@angular/core';
import { LocalstorageService } from '../localstorage.service';
import { Router } from '@angular/router';
import { MatPaginatorIntl, MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {

  displayedColumns: string[] = ['name', 'phone', 'email', 'actions'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private localStorageService: LocalstorageService, private router: Router) {
    this.dataSource = new MatTableDataSource();
  }

  ngOnInit(): void {
    this.loadContacts();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  loadContacts() {
    const contacts = this.localStorageService.getContacts();
    this.dataSource.data = contacts;
    console.log(contacts);
  }

  editContact(id: number) {
    this.router.navigate([`/edit/${id}`]);
  }

  deleteContact(id: number) {
    if (confirm('Are you sure you want to delete this contact?')) {
      this.localStorageService.deleteContact(id);
      this.loadContacts();
    }
  }

  onAddContact() {
    this.router.navigate(['/add'])
  }

}
