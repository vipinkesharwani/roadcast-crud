import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalstorageService } from '../localstorage.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {

  contactForm: FormGroup;
  isEdit: boolean = false;
  contactId: number;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router, private localStorageService: LocalstorageService) { 
      this.contactForm = this.fb.group({
        name: ['', Validators.required],
        phone: ['', [Validators.required, Validators.pattern('^[0-9]{10}$')]], 
        email: ['', [Validators.required, Validators.email]],
      });
    }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params['id']) {
        this.isEdit = true;
        this.contactId = +params['id'];
        const contact = this.localStorageService.getContact(this.contactId);
        if (contact) {
          this.contactForm.patchValue(contact);
        }
      }
    });
  }

  onSubmit() {
    if (this.contactForm.valid) {
      if (this.isEdit) {
        this.localStorageService.updateContact(this.contactId, this.contactForm.value);
      } else {
        this.localStorageService.addContact(this.contactForm.value);
      }
      this.router.navigate(['/']);
    }
  }

  redirectToList() {
    this.router.navigate(['/list']);
  }

}
